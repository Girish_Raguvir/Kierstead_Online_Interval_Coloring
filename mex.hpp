/* 
	Author: Girish Raguvir J, IIT Madras
	https://github.com/Girish-Raguvir 
*/

#ifndef ___MEX_H

#define ___MEX_H

#include <vector>
#include <iostream>
#include <math.h>

using namespace std;

class Mex
{

	private:
	
		set<int> s;
		int curr;

	public:

		Mex()
		{
			curr = 0;
		}

		void insert(int x)
		{
			s.insert(x);

			if(x==curr)
			{
				set<int>::iterator i = s.find(x);
				for (; i!=s.end(); ++i, curr++)
					if(curr<(*i)) break;
			}
		}

		int get_mex()
		{
			return curr;
		}
};

#endif