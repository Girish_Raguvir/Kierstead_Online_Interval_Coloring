/* 
	Author: Girish Raguvir J, IIT Madras
	https://github.com/Girish-Raguvir 
*/

#include <bits/stdc++.h>
	
#include "KT_Tree.hpp"
#include "mex.hpp"

using namespace std;

#define FOR(i, a, b) for(int i = (a); i < (b); ++i)
#define RFOR(i, b, a) for(int i = (b) - 1; i >= (a); --i)
#define FILL(A,value) memset(A,value,sizeof(A))

#define all(V) V.begin(), V.end()
#define pb push_back
#define mp make_pair

typedef long long ll;
typedef unsigned long long llu;
typedef vector <ll> vi;
typedef long double ld;
typedef pair <ll, ll> pii;

void solve()
{
	KT_Tree<int> T;
	int l,r,n;
	int level;

	cin>>n;
	FOR(i,0,n)
	{
		cin>>l>>r;
		cout<<l<<" "<<r<<endl;
		level = T.insert(new Interval<int>(l,r));
		cout<<"Level: "<<level<<endl<<endl;
	}
}

int main() 
{
	solve();
	return 0;
}   

