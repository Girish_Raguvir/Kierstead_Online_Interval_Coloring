/* 
	Author: Girish Raguvir J, IIT Madras
	https://github.com/Girish-Raguvir 
*/

#ifndef ___KT_TREE_H

#define ___KT_TREE_H

#include <vector>
#include <iostream>
#include <math.h>

#include "mex.hpp"

using namespace std;

template <typename T>
class Interval
{
	public:

		T start;
		T end;
		
		Interval(T s, T e): start(s), end(e) {}
		template<T> friend ostream& operator<<(ostream& os, const Interval<T> &t);
};

template <typename T>
ostream& operator<<(ostream& os, const Interval<T> &t)
{
	return os<<"["<<t.start<<","<<t.end<<"]";
};

template <typename T>
class Node
{
	public:

	  	Interval<T>* i;
	  	T max_val;
	  	Node<T>* left;
	  	Node<T>* right;

	  	Node(T start, T end)
	  	{
	  		this->i = new Interval<T>(start, end);
	  		this->max_val = end;
	  		this->left = NULL;
	  		this->right = NULL;
	  	} 
};

template <typename T>
class IntervalNode : public Node<T>
{
  	public:

	  	int level;

	  	IntervalNode(T start, T end)
        : Node<T>(start, end)
	  	{
	  		this->level = 0;
	  	} 
};

template <typename T>
class EdgePoint : public Node<T>
{
  	public:

	  	Mex* supporting_line;

	  	EdgePoint(T x)
        : Node<T>(x, x)
	  	{
	  		this->supporting_line = new Mex();	
	  	}

	  	int get_height_of_supporting_line()
	  	{
	  		supporting_line->get_mex();
	  	}

	  	void update_supporting_line(int level)
		{
			supporting_line->insert(level);
		}
};

template <typename T>
class KT_Tree 
{	
	private:
		
		EdgePoint<T>* edgepoints_root;
		IntervalNode<T>* intervals_root;

	public:

		KT_Tree()
		{
			edgepoints_root = NULL;
			intervals_root = NULL;
		}

		int insert(Interval<T>* t)
		{
			EdgePoint<T>* left = new EdgePoint<T>(t->start);
			EdgePoint<T>* right = new EdgePoint<T>(t->end);

			EdgePoint<T>* left_end = node_with_supporting_line(intervals_root, left);
			EdgePoint<T>* right_end = node_with_supporting_line(intervals_root, right);

			cout<<"-----------"<<endl;
			cout<<"Left end: "<<(left_end->get_height_of_supporting_line())<<endl;
			cout<<"Right end: "<<(right_end->get_height_of_supporting_line())<<endl;
			cout<<"-----------"<<endl;

			edgepoints_root = (EdgePoint<T>*) insert_node(edgepoints_root, left_end);
			edgepoints_root = (EdgePoint<T>*) insert_node(edgepoints_root, right_end);

			int level = find_level(edgepoints_root, t);
			update_edge_points(edgepoints_root, t, level);

			IntervalNode<T>* I = new IntervalNode<T>(t->start, t->end);
			I->level = level;
			intervals_root = (IntervalNode<T>*) insert_node(intervals_root, I);

			return level;
		}

	private:

		bool check_overlap(Interval<T>* a, Interval<T>* b)
		{
			if(a->start<= b->end && b->start <= a->end) return true;
			else return false;
		}


		EdgePoint<T>* node_with_supporting_line(IntervalNode<T>*r, EdgePoint<T>* t)
		{
			IntervalNode<T>* curr = r;

			while(curr!=NULL)
			{
				if(check_overlap(curr->i, t->i)) t->update_supporting_line(curr->level);

				if(curr->left!=NULL && t->i->start<=curr->left->max_val) curr = (IntervalNode<T>*) curr->left;
				else curr = (IntervalNode<T>*) curr->right;
			}

			return t;
		}

		void update_edge_points(EdgePoint<T>* r, Interval<T>* t, int level)
		{
			EdgePoint<T>* curr = r;

			while(curr!=NULL)
			{
				if(check_overlap(curr->i,t)) curr->update_supporting_line(level);

				if(curr->left!=NULL && t->start<=curr->left->max_val) curr = (EdgePoint<T>*) curr->left;
				else curr = (EdgePoint<T>*) curr->right;
			}
		}

		int find_level(EdgePoint<T>* r, Interval<T>* t)
		{
			EdgePoint<T>* curr = r;
			int h = 0;

			while(curr!=NULL)
			{
				if(check_overlap(curr->i,t)) 
				{
					int hs = curr->get_height_of_supporting_line();
					h = max(hs,h);
				}

				if(curr->left!=NULL && t->start<=curr->left->max_val) curr = (EdgePoint<T>*) curr->left;
				else curr = (EdgePoint<T>*) curr->right;
			}

			return h;
		}

		Node<T>* insert_node(Node<T>* r, Node<T>* t)
		{
			if(r==NULL) return t;
			
			T k = r->i->start;
			if(t->i->start < k) r->left = insert_node(r->left, t);
			else r->right = insert_node(r->right, t);

			if(r->max_val < t->i->end) r->max_val = t->i->end;

			return r;
		}
};

#endif
